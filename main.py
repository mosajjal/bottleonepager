#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bottle import response as req_response
from bottle import request, route, run, static_file, template


@route(path="/static/<filename:path>")
def send_static(filename):
    return static_file(filename, root="static/")


@route(path="/", method=["POST", "GET"])
def index():
    if request.method == "GET":
        return template("templates/index.html")

if __name__ == "__main__":
    run(host="127.0.0.1", port=8942, debug=True)
